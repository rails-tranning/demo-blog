Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    #
    namespace :admin do
        root to: 'dashboard#index'
        get :sign_in, to: 'session#new'
        post :login, to: 'session#login'
        delete :logout, to: 'session#logout'
        resources :articles
    end
end
