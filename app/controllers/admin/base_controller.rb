class Admin::BaseController < ::ApplicationController
    layout 'admin'
    class AdminResourceError < StandardError; end

    rescue_from AdminResourceError, with: :handle_auth_error

    def handle_auth_error err={}
        redirect_to admin_sign_in_url, notice: err.message
    end

    helper_method [:current_user]

    def current_user
        @user ||= User.find_by id: session[:user_id]
    end

    def signin params={}
        user = User.find_by_email params[:email]
        raise AdminResourceError, "邮箱找不到" unless user
        user = user.authenticate params[:password]
        raise AdminResourceError, "密码错误" unless user
        session[:user_id] = user.id
    end

    def logout
        session[:user_id] = nil
    end
end
