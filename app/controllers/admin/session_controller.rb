class Admin::SessionController < Admin::BaseController
    layout 'login'
    def new
        @user = User.new
    end

    def login
        signin session_params
        redirect_to admin_root_url, notice: "登录成功"
    end

    def logout
    end

    private

    def session_params
        params.fetch(:session, {}).permit(:email, :password)
    end
end
